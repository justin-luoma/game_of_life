use crate::cell_state::CellState;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub struct Cell {
    pub id: u32,
    pub state: CellState,
}
