use std::error::Error;
use std::io::stdout;
use std::time::Duration;

use crossterm::cursor::{Hide, MoveTo, Show};
use crossterm::event::{poll, read, Event};
use crossterm::execute;
use crossterm::style::{Color, Print, ResetColor, SetForegroundColor};
use crossterm::terminal::{Clear, ClearType, EnterAlternateScreen, LeaveAlternateScreen};

use game_of_life_api::grid::Grid;

fn main() -> Result<(), Box<dyn Error>> {
    let mut grid = Grid::new(25, 25);
    grid.set_cells_alive(vec![(1, 0), (2, 1), (0, 2), (1, 2), (2, 2)]);

    execute!(
        stdout(),
        EnterAlternateScreen,
        SetForegroundColor(Color::Magenta),
        Hide
    )?;

    loop {
        if poll(Duration::from_millis(500))? {
            if let Event::Key(_) = read()? {
                break;
            }
        } else {
            execute!(
                stdout(),
                Clear(ClearType::All),
                MoveTo(0, 0),
                Print(&grid),
                Print("Press enter to exit..."),
            )?;
            grid.step_forward();
        }
    }
    execute!(stdout(), ResetColor, Show, LeaveAlternateScreen)?;

    Ok(())
}
