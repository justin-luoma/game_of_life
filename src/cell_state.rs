#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum CellState {
    Alive,
    Dead,
}
